package com.Mapper;

import java.util.HashSet;
import java.util.Set;

import com.choice.DTO.AddressDTO;
import com.choice.DTO.DepartmentDTO;
import com.choice.DTO.EmployeeDTO;
import com.choice.DTO.OrganizationDTO;
import com.choice.model.Address;
import com.choice.model.Department;
import com.choice.model.Employee;
import com.choice.model.Organization;

public class MapStructMapperImpl {
	
	public static Organization convertToOrganization(OrganizationDTO organizationDTO) {
        if ( organizationDTO == null ) {
            return null;
        }

        Organization organization = new Organization();

        organization.setId( organizationDTO.getId() );
        organization.setName( organizationDTO.getName() );
        organization.setPhone_num( organizationDTO.getPhone_num() );

        return organization;
    }
	
	public static Set<Department> convertToDepartment(DepartmentDTO departmentDTO) {
        if ( departmentDTO == null ) {
            return null;
        }

        Department department = new Department();
        department.setName( departmentDTO.getDepartment_name() );
        department.setCode(departmentDTO.getDepartment_code());
        
        //organization.setPhone_num( departmentDTO.ge() );

        Set<Department> deptSet = new HashSet();
        deptSet.add(department);
        return deptSet;
    }
	
	public static Department convertToDepartment1(DepartmentDTO departmentDTO) {
        if ( departmentDTO == null ) {
            return null;
        }

        Department department = new Department();
        department.setName( departmentDTO.getDepartment_name() );
        department.setCode(departmentDTO.getDepartment_code());
        
        return department;
    }

	public static Employee convertToEmployee(EmployeeDTO employeeDTO) {
        if ( employeeDTO == null ) {
            return null;
        }

        Employee employee = new Employee();

        employee.setId( employeeDTO.getId() );
        employee.setEmployee_num( employeeDTO.getEmployee_num() );
        employee.setFirst_name( employeeDTO.getFirst_name() );
        employee.setLast_name( employeeDTO.getLast_name() );
        employee.setDepartment_code( employeeDTO.getDepartment_code() );
        employee.setEmail( employeeDTO.getEmail() );
        employee.setAge( employeeDTO.getAge() );
        return employee;
    }
	
	public static Set<Address> convertToAddress(AddressDTO addressDTO) {
        if ( addressDTO == null ) {
            return null;
        }

        Address address = new Address();

        address.setAddress1( addressDTO.getAddress1() );
        address.setAddress2( addressDTO.getAddress2() );
        address.setCity( addressDTO.getCity() );
        address.setState( addressDTO.getState() );
        address.setZipCode( addressDTO.getZipCode() );
        
        Set<Address> addressSet = new HashSet<Address>();
        addressSet.add(address);
        return addressSet;
    }
}
