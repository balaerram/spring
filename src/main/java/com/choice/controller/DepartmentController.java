package com.choice.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Mapper.MapStructMapperImpl;
import com.choice.DTO.DepartmentDTO;
import com.choice.exception.ServiceException;
import com.choice.model.Department;
import com.choice.service.DepartmentService;

@RestController
public class DepartmentController {

	@Autowired
	DepartmentService deptService;

	@GetMapping("/department")
	private List<Department> getDepartments() {
		return deptService.getDepartments();
	}

	@GetMapping("/department/{name}")
	private Department getDepartment(@PathVariable("name") String name) throws ServiceException {
		return deptService.getDepartmentByName(name);
	}

	@DeleteMapping("/department/{id}")
	private void deleteDepartment(@PathVariable("id") int id) {
		deptService.delete(id);
	}

	@PostMapping("/{orgId}/department")
	private Department savedepartment(@PathVariable int orgId,
			@Valid @NotNull @RequestBody DepartmentDTO departmentDTO) throws ServiceException {
		return deptService.saveOrUpdate(orgId, MapStructMapperImpl.convertToDepartment1(departmentDTO));
	}
}
