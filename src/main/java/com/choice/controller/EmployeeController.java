package com.choice.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Mapper.MapStructMapperImpl;
import com.choice.DTO.EmployeeDTO;
import com.choice.exception.ServiceException;
import com.choice.model.Employee;
import com.choice.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@GetMapping("/employee")
	private List<Employee> getEmployees() {
		return employeeService.getEmployees();
	}

	@GetMapping("/employee/{id}")
	private Employee getEmployee(@PathVariable("id") int id) {
		return employeeService.getEmployeeById(id);
	}
	
	@GetMapping("/employee/{first_name}")
	private Employee getEmployeeByName(@PathVariable("name") String name) {
		return employeeService.getEmployeeByName(name);
	}
	
	@GetMapping("/employee/{employee_num}")
	private Employee getEmployeeByCode(@PathVariable("name") String employee_num) {
		return employeeService.getEmployeeByNumber(employee_num);
	}
	
	@GetMapping("/employee/{email}")
	private Employee getEmployeeByEmail(@PathVariable("name") String email) {
		return employeeService.getEmployeeByEmail(email);
	}
	
	@PostMapping("/{deptId}/employee")
	private Employee saveEmployee(@PathVariable int deptId, @Valid @NotNull @RequestBody EmployeeDTO employeeDTO) throws ServiceException {
		Employee employee = MapStructMapperImpl.convertToEmployee(employeeDTO);
		employee.setAddress(MapStructMapperImpl.convertToAddress(employeeDTO.getAddress()));
		Employee emp = employeeService.saveOrUpdate(deptId, employee);
		return emp;
	}
	
	@PutMapping("/employee/{empId}")
	private Employee update(@PathVariable int empId, @Valid @NotNull @RequestBody EmployeeDTO employeeDTO) {
		Employee employee = MapStructMapperImpl.convertToEmployee(employeeDTO);
		employee.setAddress(MapStructMapperImpl.convertToAddress(employeeDTO.getAddress()));
		employee.setId(empId);
		Employee emp = employeeService.update(employee);
		return emp;
	}
	
	@DeleteMapping("/employee/{id}")
	private void deleteEmployee(@PathVariable("id") int id) {
		employeeService.delete(id);
	}
}
