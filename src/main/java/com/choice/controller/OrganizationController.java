package com.choice.controller;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Mapper.MapStructMapperImpl;
import com.choice.DTO.OrganizationDTO;
import com.choice.exception.ServiceException;
import com.choice.model.Organization;
import com.choice.service.OrganizationService;

@RestController
public class OrganizationController {

	@Autowired
	OrganizationService organizationService;

	private static final Logger logger = Logger.getLogger(OrganizationController.class.getName());

	@PostMapping("/organization")
	private Organization saveOrganizationService(@Valid @NotNull @RequestBody OrganizationDTO organization) throws ServiceException {
		logger.entering(OrganizationController.class.getName(), "saveOrganizationService");
		Organization org = MapStructMapperImpl.convertToOrganization(organization);
		organizationService.saveOrUpdate(org);
		logger.log(java.util.logging.Level.INFO, "organization id {0}", org.getId());
		return org;
	}

	@GetMapping("/organization/{name}")
	private Organization getOrganizationServiceByName(@PathVariable("name") String name) throws ServiceException {
		logger.entering(OrganizationController.class.getName(), "getOrganizationServiceByName");
		return organizationService.getOrganizationServiceByName(name);
	}

	@GetMapping("/organization/phone/{phone-num}")
	private List<Organization> getOrganizationServiceByPhone(@PathVariable("phone-num") String phone) throws ServiceException {
		logger.entering(OrganizationController.class.getName(), "getOrganizationServiceByPhone");
		return organizationService.getOrganizationServiceByPhone(phone);
	}
}
