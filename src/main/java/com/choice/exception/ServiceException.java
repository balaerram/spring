package com.choice.exception;

public class ServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	private String message;
	private String details;
	private String statusCode;

	protected ServiceException() {
	}

	public ServiceException(String message, String details) {
		this.message = message;
		this.details = details;
	}

	public ServiceException(String message, String details, String statusCode) {
		this.message = message;
		this.details = details;
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

}
