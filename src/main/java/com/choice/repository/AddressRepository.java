package com.choice.repository;

import org.springframework.data.repository.CrudRepository;

import com.choice.model.Address;

public interface AddressRepository extends CrudRepository<Address, Integer>
{
}
