package com.choice.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.choice.model.Department;

public interface DepartmentRepository extends CrudRepository<Department, Integer>
{
	@Query("Select d from Department d where d.name= ?1")
	public Department searchByName(String name);
	
	@Query("Select d from Department d where d.code= ?1")
	public Department searchByCode(String code);
}
