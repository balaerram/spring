package com.choice.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.choice.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>
{
	@Query("Select e from Employee e where e.first_name= ?1")
	public Employee searchByName(String name);
	
	@Query("Select e from Employee e where e.employee_num= ?1")
	public Employee searchByCode(String employee_num);
	
	@Query("Select e from Employee e where e.email= ?1")
	public Employee searchByEmail(String email);
}
