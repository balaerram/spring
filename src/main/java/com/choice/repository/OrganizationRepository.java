package com.choice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.choice.model.Organization;

public interface OrganizationRepository extends CrudRepository<Organization, Integer> {

	@Query("Select o from Organization o where o.name= ?1")
	public Organization searchByName(String name);
	
	@Query("Select o from Organization o where o.phone_num= ?1")
	public List<Organization> searchByPhone(String phone);
}


