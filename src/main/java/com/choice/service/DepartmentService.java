package com.choice.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.exception.ServiceException;
import com.choice.model.Department;
import com.choice.model.Organization;
import com.choice.repository.DepartmentRepository;
import com.choice.repository.OrganizationRepository;

@Service
public class DepartmentService {
	@Autowired
	DepartmentRepository departmentRepository;

	@Autowired
	OrganizationRepository orgRepository;

	public List<Department> getDepartments() {
		List<Department> departments = new ArrayList<Department>();
		departmentRepository.findAll().forEach(department -> departments.add(department));
		return departments;
	}

	public Department getDepartmentById(int id) {
		return departmentRepository.findById(id).get();
	}

	public Department getDepartmentByName(String name) throws ServiceException {
		try {
			return departmentRepository.searchByName(name);
		} catch (Exception e) {
			throw new ServiceException("NO Value Present in DB", "NO Value Present in DB first insert the data");
		}
	}

	public Department saveOrUpdate(int orgId, Department department) throws ServiceException{
		Optional<Organization> organizationId = orgRepository.findById(orgId);
		if (!organizationId.isPresent()) {
			throw new ServiceException("Organization with id {0} does not exist" +  orgId, "", "404");
		}
		Organization org = organizationId.get();
		department.setOrganization(org);
		department.setCreatedAt(new Date());
		department.setUpdatedAt(new Date());
		return departmentRepository.save(department);
	}

	public void delete(int id) {
		departmentRepository.deleteById(id);
	}
}