package com.choice.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.exception.ServiceException;
import com.choice.model.Department;
import com.choice.model.Employee;
import com.choice.repository.DepartmentRepository;
import com.choice.repository.EmployeeRepository;

@Service
public class EmployeeService 
{
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	DepartmentRepository departmentRepository;

	public List<Employee> getEmployees() 
	{
		List<Employee> employees = new ArrayList<Employee>();
		employeeRepository.findAll().forEach(employee -> employees.add(employee));
		return employees;
	}
	
	public Employee getEmployeeByName(String name)
	{
		return employeeRepository.searchByName(name);
	}
	
	public Employee getEmployeeByNumber(String empNum)
	{
		return employeeRepository.searchByCode(empNum);
	}
	
	public Employee getEmployeeByEmail(String email)
	{
		return employeeRepository.searchByEmail(email);
	}
	
	public Employee getEmployeeById(int id) 
	{
		return employeeRepository.findById(id).get();
	}
	
	public Employee saveOrUpdate(int deptId, Employee employee) throws ServiceException 
	{
		Optional<Department> departmentId = departmentRepository.findById(deptId);
		if(!departmentId.isPresent()) {
			throw new ServiceException("Department with id " + deptId + " does not exist","");
        }
		
		Department dept = departmentId.get();
		employee.setDepartment(dept);
		
		employee.setCreatedAt(new Date());
		employee.setUpdatedAt(new Date());
		return employeeRepository.save(employee);
	}
	
	public Employee update(Employee employee) 
	{
		
		Employee emp = employeeRepository.findById(employee.getId()).get();
		
		emp.setEmployee_num( employee.getEmployee_num() );
		emp.setFirst_name( employee.getFirst_name() );
		emp.setLast_name( employee.getLast_name() );
		emp.setDepartment_code( employee.getDepartment_code() );
		emp.setEmail( employee.getEmail() );
		emp.setAge( employee.getAge() );
		emp.setCreatedAt(new Date());
		emp.setUpdatedAt(new Date());
		return employeeRepository.save(emp);
	}
	
	public void delete(int id) 
	{
		employeeRepository.deleteById(id);
	}
}