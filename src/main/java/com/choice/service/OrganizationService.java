package com.choice.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.exception.ServiceException;
import com.choice.model.Organization;
import com.choice.repository.OrganizationRepository;

@Service
public class OrganizationService {
	
	@Autowired
	OrganizationRepository organizationRepository;

	public void saveOrUpdate(Organization organization) throws ServiceException {

		Organization org = getOrganizationServiceByName(organization.getName());
		if (org != null) {
			throw new ServiceException("Ogranization already exists", "Ogranization connot be added mroe than once");
		}

		organization.setCreatedAt(new Date());
		organization.setUpdatedAt(new Date());
		organizationRepository.save(organization);
	}
	
	public Organization getOrganizationServiceByName(String name) throws ServiceException{
		return organizationRepository.searchByName(name);
	}
	
	public Optional<Organization> getOrganizationServiceById(int id) throws ServiceException{
		return organizationRepository.findById(id);
}
	
	public List<Organization> getOrganizationServiceByPhone(String phone) throws ServiceException{
		try {
			return organizationRepository.searchByPhone(phone);
		} catch (Exception e) {
			throw new ServiceException("NO Value Present in DB", "NO Value Present in DB first insert the data");
		}
	}
}